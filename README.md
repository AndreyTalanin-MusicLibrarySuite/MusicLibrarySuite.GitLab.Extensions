# MusicLibrarySuite.GitLab.Extensions

A repository containing the GitLab CI/CD Catalog and templates of files reused by other repositories within the [MusicLibrarySuite](https://gitlab.com/AndreyTalanin-MusicLibrarySuite) group.
